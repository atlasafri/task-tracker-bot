.PHONY: build
build:
	go mod vendor
	go build -mod=vendor -o bin/bot ./bot

.PHONY: test
test:
	go test -mod=vendor ./bot

.PHONY: lint
lint:
	go mod vendor
	golangci-lint run -c .golangci.yml -v --modules-download-mode=vendor ./bot/...
