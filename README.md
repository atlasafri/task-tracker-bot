* `/tasks` - show all tasks
* `/new XXX YYY ZZZ` - create new task
* `/assign_$ID` - assign task to current user by task id
* `/unassign_$ID` - unassign task from current user by task id
* `/resolve_$ID` - deletes task by id
* `/my` - show tasks assigned to current user
* `/owner` - show tasks created by current user