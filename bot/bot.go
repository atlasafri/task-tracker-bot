package main

// сюда писать код

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	TaskNotFound = "Задачи с таким id не существует"
	NoTasks      = "Нет задач"
)

var (
	BotToken   = flag.String("tg.token", "", "token for telegram")
	WebhookURL = flag.String("tg.webhook", "", "webhook addr for telegram")
	TimedMode  = flag.Bool("timed-mode", false, "timed shutdown")
)

type Secrets struct {
	BotToken   string `json:"bot_token"`
	WebhookURL string `json:"webhook_url"`
	TimedMode  bool   `json:"timed_mode"`
}

type Task struct {
	ID          int
	Description string
	Creator     *tgbotapi.User
	Assignee    *tgbotapi.User
}

type ChatInfo struct {
	Chat *tgbotapi.Chat
	User *tgbotapi.User
}

var nextID int

var botChats map[int64]ChatInfo

var allTasks map[int]*Task

var bot *tgbotapi.BotAPI

func tasks(userID int64) string {
	if len(allTasks) == 0 {
		return NoTasks
	}
	res := make([]string, 0)
	for _, task := range allTasks {
		entry := fmt.Sprintf("%d. %s by @%s", task.ID, task.Description, task.Creator.UserName)
		if task.Assignee != nil {
			if task.Assignee.ID == userID {
				entry += fmt.Sprintf("\nassignee: я\n/unassign_%d /resolve_%d", task.ID, task.ID)
			} else {
				entry += fmt.Sprintf("\nassignee: @%s", task.Assignee.UserName)
			}
		} else {
			entry += fmt.Sprintf("\n/assign_%d", task.ID)
		}
		res = append(res, entry)
	}
	return strings.Join(res, "\n\n")
}

func newTask(inp string, userID int64) string {
	allTasks[nextID] = &Task{
		ID:          nextID,
		Description: inp,
		Creator:     botChats[userID].User,
	}
	res := fmt.Sprintf("Задача \"%s\" создана, id=%d", inp, nextID)
	nextID++
	return res
}

func assign(taskID int, userID int64) (string, error) {
	task, found := allTasks[taskID]
	if found {
		if task.Assignee == nil && task.Creator != task.Assignee && task.Creator.ID != userID {
			_, err := bot.Send(tgbotapi.NewMessage(
				botChats[task.Creator.ID].Chat.ID,
				fmt.Sprintf("Задача \"%s\" назначена на @%s", task.Description, botChats[userID].User.UserName),
			))
			if err != nil {
				return "", err
			}
		}

		if task.Assignee != nil && task.Assignee.ID != userID {
			_, err := bot.Send(tgbotapi.NewMessage(
				botChats[task.Assignee.ID].Chat.ID,
				fmt.Sprintf("Задача \"%s\" назначена на @%s", task.Description, botChats[userID].User.UserName),
			))
			if err != nil {
				return "", err
			}
		}

		if task.Assignee != nil && task.Assignee.ID == userID {
			return fmt.Sprintf("Задача \"%s\" уже назначена на вас", task.Description), nil
		}

		task.Assignee = botChats[userID].User
		return fmt.Sprintf("Задача \"%s\" назначена на вас", task.Description), nil
	}
	return TaskNotFound, nil
}

func unassign(taskID int, userID int64) (string, error) {
	task, found := allTasks[taskID]
	if found {
		if task.Assignee.ID == userID {
			task.Assignee = nil
			_, err := bot.Send(tgbotapi.NewMessage(
				botChats[task.Creator.ID].Chat.ID,
				fmt.Sprintf("Задача \"%s\" осталась без исполнителя", task.Description),
			))
			if err != nil {
				return "", err
			}
			return "Принято", nil
		}

		return "Задача не на вас", nil
	}
	return TaskNotFound, nil
}

func resolve(taskID int, userID int64) (string, error) {
	task, found := allTasks[taskID]

	if found {
		res := fmt.Sprintf("Задача \"%s\" выполнена", task.Description)
		forCreator := res
		if task.Assignee != nil {
			forCreator = fmt.Sprintf("%s @%s", forCreator, task.Assignee.UserName)
		}

		if task.Creator.ID != userID {
			_, err := bot.Send(tgbotapi.NewMessage(
				botChats[task.Creator.ID].Chat.ID,
				forCreator,
			))
			if err != nil {
				return "", err
			}
		}

		delete(allTasks, taskID)
		return res, nil
	}
	return TaskNotFound, nil
}

func my(userID int64) string {
	res := make([]string, 0)
	for _, task := range allTasks {
		if task.Assignee != nil && task.Assignee.ID == userID {
			entry := fmt.Sprintf(
				"%d. %s by @%s\n/unassign_%d /resolve_%d",
				task.ID,
				task.Description,
				task.Creator.UserName,
				task.ID,
				task.ID,
			)
			res = append(res, entry)
		}
	}
	if len(res) == 0 {
		return NoTasks
	}
	return strings.Join(res, "\n\n")
}

func owner(userID int64) string {
	res := make([]string, 0)
	for _, task := range allTasks {
		if task.Creator == nil || task.Creator.ID != userID {
			continue
		}
		entry := fmt.Sprintf(
			"%d. %s by @%s\n/assign_%d",
			task.ID,
			task.Description,
			task.Creator.UserName,
			task.ID,
		)
		res = append(res, entry)
	}
	if len(res) == 0 {
		return NoTasks
	}
	return strings.Join(res, "\n\n")
}

func runCommand(cmd string, id int, inp string, chatID int64, userID int64) error {
	var res string
	var err error
	switch cmd {
	case "/tasks":
		res = tasks(userID)
	case "/new":
		res = newTask(inp, userID)
	case "/assign":
		res, err = assign(id, userID)
	case "/unassign":
		res, err = unassign(id, userID)
	case "/resolve":
		res, err = resolve(id, userID)
	case "/my":
		res = my(userID)
	case "/owner":
		res = owner(userID)
	default:
		res = "unknown command"
	}
	if err != nil {
		return err
	}

	// sending command result as message
	msg := tgbotapi.NewMessage(
		chatID,
		res,
	)

	// keyboard for commands that take no arguments
	msg.ReplyMarkup = &tgbotapi.ReplyKeyboardMarkup{
		Keyboard: [][]tgbotapi.KeyboardButton{
			{
				{Text: "/tasks"},
				{Text: "/my"},
				{Text: "/owner"},
			},
		},
	}
	_, err = bot.Send(msg)
	if err != nil {
		return err
	}
	return nil
}

func listenForUpdates(ctx context.Context, server *http.Server, updates tgbotapi.UpdatesChannel) error {
	for {
		select {
		case <-ctx.Done():
			err := server.Shutdown(ctx)
			if err != nil {
				log.Println("error on shutdown:", err)
			}
			return nil
		case update := <-updates:
			if update.Message == nil {
				log.Printf("upd with no message: %#v\n", update)
				continue
			}
			// processing every message received
			var err error
			var id int
			log.Printf("upd: %#v\n", update.Message.Text)

			userID := update.Message.From.ID

			botChats[userID] = ChatInfo{
				Chat: update.Message.Chat,
				User: update.Message.From,
			}

			msgText := update.Message.Text
			command, inp, _ := strings.Cut(msgText, " ")
			cmd, sID, foundID := strings.Cut(command, "_")
			if foundID {
				id, err = strconv.Atoi(sID)
				if err != nil {
					log.Printf("strconv.Atoi(sID) failed: %s", err)
					cmd = command
				}
			}

			err = runCommand(cmd, id, inp, update.Message.Chat.ID, userID)
			if err != nil {
				log.Printf("error while running %s: %s", cmd, err)
				return err
			}
		}
	}
}
func startTaskBot(ctx context.Context) error {
	// server setup for Telegram bot API
	var err error
	flag.Parse()

	nextID = 1
	botChats = make(map[int64]ChatInfo)
	allTasks = make(map[int]*Task)

	bot, err = tgbotapi.NewBotAPI(*BotToken)
	if err != nil {
		log.Printf("NewBotAPI failed: %s", err)
		return err
	}

	bot.Debug = true
	log.Printf("Authorized on account %s\n", bot.Self.UserName)

	wh, err := tgbotapi.NewWebhook(*WebhookURL)
	if err != nil {
		log.Printf("NewWebhook failed: %s", err)
		return err
	}

	_, err = bot.Request(wh)
	if err != nil {
		log.Printf("SetWebhook failed: %s", err)
		return err
	}

	updates := bot.ListenForWebhook("/")

	http.HandleFunc("/state", func(w http.ResponseWriter, r *http.Request) {
		_, err = w.Write([]byte("all is working"))
		if err != nil {
			log.Printf("write failed: %s", err)
		}
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8001"
	}
	server := &http.Server{Addr: ":" + port, Handler: nil}

	go func() {
		log.Println("http err:", server.ListenAndServe())
	}()

	return listenForUpdates(ctx, server, updates)
}

// func useLogFile(filename string) *os.File {
// 	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
// 	if err != nil {
// 		log.Fatalf("error opening log file: %v", err)
// 	}

//		log.SetOutput(f)
//		return f
//	}
func main() {
	// logfile := useLogFile("bot.log")
	// defer logfile.Close()

	// setting up context to cancel on Interrupt signal (ctrl+C)
	var ctx context.Context
	var cancel context.CancelFunc
	if *TimedMode {
		ctx, cancel = context.WithTimeout(context.Background(), 30*time.Second)
	} else {
		ctx, cancel = context.WithCancel(context.Background())
	}

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)
	defer func() {
		signal.Stop(signalChan)
		cancel()
	}()
	go func() {
		select {
		case <-signalChan: // first signal, cancel context
			cancel()
		case <-ctx.Done():
		}
		<-signalChan // second signal, hard exit
		os.Exit(2)
	}()

	// starting the bot
	err := startTaskBot(ctx)
	if err != nil {
		log.Printf("faled to start bot: %s", err)
	}
}
